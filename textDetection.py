import cv2
import pytesseract
import documentScanner as ds

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'
img = cv2.imread('resources/dui.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# print(pytesseract.image_to_string(img))


# DETECTING CHARACTERS
def detectChar(img):
    print('Detecting Characters...')
    hImg, wImg, _ = img.shape
    boxes = pytesseract.image_to_boxes(img)
# This prints the coordinates of every character
# print(boxes)
    for b in boxes.splitlines():
        # print(b)
        b = b.split(' ')
        x, y, w, h = int(b[1]), int(b[2]), int(b[3]), int(b[4])
        cv2.rectangle(img, (x, hImg - y), (w, hImg - h), (0, 0, 255), 2)
        cv2.putText(img, b[0], (x, hImg - y + 35), cv2.FONT_HERSHEY_COMPLEX, 1, (50, 50, 255), 2)

    return img


# DETECTING WORDS
def detectWords(img):
    print('Detecting Words...')
    hImg, wImg, _ = img.shape
    boxes = pytesseract.image_to_data(img)
# This prints the information of every string
# print(boxes)
    for x, b in enumerate(boxes.splitlines()):
        if x != 0:
            b = b.split()
            print(b)
            if len(b) == 12:
                x, y, w, h = int(b[6]), int(b[7]), int(b[8]), int(b[9])
                cv2.rectangle(img, (x, y), (w+x, h+y), (0, 0, 255), 2)
                cv2.putText(img, b[11], (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (50, 50, 255), 2)
    return img


# DETECTING NUMBERS
def detectNumbers(img):
    print('Detecting Numbers...')
    hImg, wImg, _ = img.shape
    cong = r'--oem 0 --psm 6 outputbase digits'
    boxes = pytesseract.image_to_data(img, config=cong)
# This prints the information of every string
# print(boxes)
    for x, b in enumerate(boxes.splitlines()):
        if x != 0:
            b = b.split()
            if len(b) == 12:
                # print(b)
                x, y, w, h = int(b[6]), int(b[7]), int(b[8]), int(b[9])
                cv2.rectangle(img, (x, y), (w+x, h+y), (0, 0, 255), 2)
                cv2.putText(img, b[11], (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (50, 50, 255), 2)
    return img


# imgChar = img.copy()
imgWords = img.copy()
# imgNumbers = img.copy()
# imgChar = detectChar(imgChar)
imgWords = detectWords(imgWords)
# imgNumbers = detectNumbers(imgNumbers)
# cv2.imshow('Characters', imgChar)
cv2.imshow('Words', imgWords)
# cv2.imshow('Numbers', imgNumbers)
cv2.waitKey(0)





